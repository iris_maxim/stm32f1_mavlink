#include "UART.h"


#define TXBUFFERSIZE   	(64*32) // 1 KByte
#define RXBUFFERSIZE   	(64*32)


/* fill output buffers with some asciis to start with */
uint8_t usart1_tx_buffer[TXBUFFERSIZE] = "";
uint8_t usart1_rx_buffer[RXBUFFERSIZE] = "";
uint8_t usart2_tx_buffer[TXBUFFERSIZE] = "";
uint8_t usart2_rx_buffer[RXBUFFERSIZE] = "";

uint32_t usart1_tx_counter_read = 0;
uint32_t usart1_tx_counter_write = 0;
uint32_t usart1_rx_counter_read = 0;
uint32_t usart1_rx_counter_write = 0;

uint32_t usart2_tx_counter_read = 0;
uint32_t usart2_tx_counter_write = 0;
uint32_t usart2_rx_counter_read = 0;
uint32_t usart2_rx_counter_write = 0;


/**
  * @brief  Push one byte to ringbuffer of USART1
  */
uint8_t usart1_tx_ringbuffer_push(const uint8_t* ch, uint8_t len)
{
	USART_ITConfig(USART1, USART_IT_TXE, DISABLE);

	/* if there is free space in buffer */
	if ((((usart1_tx_counter_read - usart1_tx_counter_write) - 1) + TXBUFFERSIZE) % TXBUFFERSIZE > len)
	{
		uint8_t i;
		for (i = 0; i < len; i++)
		{
			usart1_tx_buffer[usart1_tx_counter_write] = ch[i];
			usart1_tx_counter_write = (usart1_tx_counter_write + 1) % TXBUFFERSIZE;
		}

		USART_ITConfig(USART1, USART_IT_TXE, ENABLE);
		return 1;
	}

	USART_ITConfig(USART1, USART_IT_TXE, ENABLE);
	return 0;
}

/**
  * @brief  Check character availability USART1
  */
int usart1_char_available(void)
{
	return (usart1_rx_counter_read != usart1_rx_counter_write);
}

/**
  * @brief  Pop one byte from ringbuffer of USART1
  */
uint8_t usart1_rx_ringbuffer_pop(void)
{
	USART_ITConfig(USART1, USART_IT_RXNE, DISABLE);

	uint8_t value = usart1_rx_buffer[usart1_rx_counter_read];
	usart1_rx_counter_read = (usart1_rx_counter_read + 1) % TXBUFFERSIZE;

	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	return value;
}

/**
  * @brief  Copy from USART2 to ringbuffer
  */
uint8_t usart1_rx_ringbuffer_push_from_usart(void)
{
	usart1_rx_buffer[usart1_rx_counter_write] = USART_ReceiveData(USART1);
	int temp = (usart1_rx_counter_write + 1) % TXBUFFERSIZE;

	if(temp == usart1_rx_counter_read)
	{
		return 0;
	}

	usart1_rx_counter_write = temp;
	return 1;
}

/**
  * @brief  Copy from ringbuffer to USART1
  */
uint8_t usart1_tx_ringbuffer_pop_to_usart(void)
{
	if (usart1_tx_counter_read != usart1_tx_counter_write)
	{
		USART_SendData(USART1, usart1_tx_buffer[usart1_tx_counter_read]);
		usart1_tx_counter_read= (usart1_tx_counter_read+1) % TXBUFFERSIZE;
		return 1;
	}
	return 0;
}



/**
  * @brief  Push one byte to ringbuffer of USART2
  */
uint8_t usart2_tx_ringbuffer_push(const uint8_t* ch, uint8_t len)
{
	USART_ITConfig(USART2, USART_IT_TXE, DISABLE);

	/* if there is free space in buffer */
	if ((((usart2_tx_counter_read - usart2_tx_counter_write) - 1) + TXBUFFERSIZE) % TXBUFFERSIZE > len)
	{
		uint8_t i;
		for (i = 0; i < len; i++)
		{
			usart2_tx_buffer[usart2_tx_counter_write] = ch[i];
			usart2_tx_counter_write = (usart2_tx_counter_write + 1) % TXBUFFERSIZE;
		}

		USART_ITConfig(USART2, USART_IT_TXE, ENABLE);
		return 1;
	}

	USART_ITConfig(USART2, USART_IT_TXE, ENABLE);
	return 0;
}

/**
  * @brief  Check character availability USART2
  */
int usart2_char_available(void)
{
	return (usart2_rx_counter_read != usart2_rx_counter_write);
}

/**
  * @brief  Pop one byte from ringbuffer of USART1
  */
uint8_t usart2_rx_ringbuffer_pop(void)
{
	USART_ITConfig(USART2, USART_IT_RXNE, DISABLE);

	uint8_t value = usart2_rx_buffer[usart2_rx_counter_read];
	usart2_rx_counter_read = (usart2_rx_counter_read + 1) % TXBUFFERSIZE;

	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
	return value;
}

/**
  * @brief  Copy from USART2 to ringbuffer
  */
uint8_t usart2_rx_ringbuffer_push_from_usart(void)
{
	usart2_rx_buffer[usart2_rx_counter_write] = USART_ReceiveData(USART2);

	int temp = (usart2_rx_counter_write + 1) % TXBUFFERSIZE;

	if(temp == usart2_rx_counter_read)
	{
		return 0;
	}
	usart2_rx_counter_write = temp;
	return 1;
}

/**
  * @brief  Copy from ringbuffer to USART2
  */
uint8_t usart2_tx_ringbuffer_pop_to_usart(void)
{
	if (usart2_tx_counter_read != usart2_tx_counter_write)
	{
		USART_SendData(USART2, usart2_tx_buffer[usart2_tx_counter_read]);
		usart2_tx_counter_read= (usart2_tx_counter_read+1) % TXBUFFERSIZE;
		return 1;
	}
	return 0;
}


/**
  * @brief  USART1 interrupt handler
  */
void USART1_IRQHandler(void)
{
	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
	{
		if(usart1_rx_ringbuffer_push_from_usart() == 0)
		{
			/* Disable the Receive interrupt if buffer is full */
			USART_ITConfig(USART1, USART_IT_RXNE, DISABLE);
		}
		return;
	}

	if(USART_GetITStatus(USART1, USART_IT_TXE) != RESET)
	{
		if(usart1_tx_ringbuffer_pop_to_usart() == 0)
		{
			/* Disable the Transmit interrupt if buffer is empty */
			USART_ITConfig(USART1, USART_IT_TXE, DISABLE);
		}

		return;
	}
}
/**
  * @brief  USART2 interrupt handler
  */
void USART2_IRQHandler(void)
{
	USART_ClearFlag(USART2,USART_FLAG_TC); 
	if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
	{

		if(usart2_rx_ringbuffer_push_from_usart() == 0)
		{
			/* Disable the Receive interrupt if buffer is full */
			USART_ITConfig(USART2, USART_IT_RXNE, DISABLE);
		}
		return;
	}

	if(USART_GetITStatus(USART2, USART_IT_TXE) != RESET)
	{
		if(usart2_tx_ringbuffer_pop_to_usart() == 0)
		{
			/* Disable the Transmit interrupt if buffer is empty */
			USART_ITConfig(USART2, USART_IT_TXE, DISABLE);
		}

		return;
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
void UART1Config(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;
  USART_InitTypeDef USART_InitStruct;

  /* USART1 Clk Init *************************************************************/
  RCC_APB2PeriphClockCmd(USART1_CLK | USART1_TX_GPIO_CLK, ENABLE); 
  /* USART1 Tx PA9 */
  GPIO_InitStruct.GPIO_Pin = USART1_TX_PIN;
  GPIO_InitStruct.GPIO_Mode = USART1_TX_MODE;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(USART1_TX_GPIO_PORT, &GPIO_InitStruct);
  /* USART1 Rx PA10 */
  GPIO_InitStruct.GPIO_Pin = USART1_RX_PIN;
  GPIO_InitStruct.GPIO_Mode = USART1_RX_MODE;
  GPIO_Init(USART1_RX_GPIO_PORT, &GPIO_InitStruct);

  /* USART1 Init *****************************************************************/
  USART_InitStruct.USART_BaudRate = USART1_BAUDRATE;
  USART_InitStruct.USART_WordLength = USART1_BYTESIZE;
  USART_InitStruct.USART_StopBits = USART1_STOPBITS;
  USART_InitStruct.USART_Parity = USART1_PARITY;
  USART_InitStruct.USART_HardwareFlowControl = USART1_HARDWARECTRL;
  USART_InitStruct.USART_Mode = USART1_Mode;
  USART_Init(USART1, &USART_InitStruct);
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
  USART_Cmd(USARTx1, ENABLE);
  }
/**************************实现函数********************************************
*函数原型:		void UART3NVIC_Configuration(void)
*功　　能:		初始化USART1中断优先级
*******************************************************************************/
void UART1NVIC_Configuration(void){

        NVIC_InitTypeDef NVIC_InitStructure; 
          /* Enable the USART1 Interrupt */
        NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
//        NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
        NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
        NVIC_Init(&NVIC_InitStructure);

}

void UART2Config(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;
  USART_InitTypeDef USART_InitStruct;

  /* USART2 Clk Init *************************************************************/
	RCC_APB1PeriphClockCmd(USART2_CLK, ENABLE);
  RCC_APB2PeriphClockCmd(USART2_TX_GPIO_CLK, ENABLE); 
	
//	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
//	GPIO_PinRemapConfig(GPIO_Remap_USART2, ENABLE);
  /* USART2 Tx PA2 */
  GPIO_InitStruct.GPIO_Pin = USART2_TX_PIN;
  GPIO_InitStruct.GPIO_Mode = USART2_TX_MODE;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(USART2_TX_GPIO_PORT, &GPIO_InitStruct);
  /* USART2 Rx PA3 */
  GPIO_InitStruct.GPIO_Pin = USART2_RX_PIN;
  GPIO_InitStruct.GPIO_Mode = USART2_RX_MODE;
  GPIO_Init(USART2_RX_GPIO_PORT, &GPIO_InitStruct);

  /* USART2 Init *****************************************************************/
  USART_InitStruct.USART_BaudRate = USART2_BAUDRATE;
  USART_InitStruct.USART_WordLength = USART2_BYTESIZE;
  USART_InitStruct.USART_StopBits = USART2_STOPBITS;
  USART_InitStruct.USART_Parity = USART2_PARITY;
  USART_InitStruct.USART_HardwareFlowControl = USART2_HARDWARECTRL;
  USART_InitStruct.USART_Mode = USART2_Mode;
  USART_Init(USART2, &USART_InitStruct);
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
  USART_Cmd(USARTx2, ENABLE);
  }
/**************************实现函数********************************************
*函数原型:		void UART3NVIC_Configuration(void)
*功　　能:		初始化USART2中断优先级
*******************************************************************************/
void UART2NVIC_Configuration(void){

        NVIC_InitTypeDef NVIC_InitStructure; 
          /* Enable the USART2 Interrupt */
        NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
//        NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
        NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
        NVIC_Init(&NVIC_InitStructure);
}

void UART3Config(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;
  USART_InitTypeDef USART_InitStruct;

  /* USART3 Clk Init *************************************************************/
	RCC_APB1PeriphClockCmd(USART3_CLK, ENABLE);
  RCC_APB2PeriphClockCmd(USART3_TX_GPIO_CLK, ENABLE); 
  /* USART3 Tx PB10 */
  GPIO_InitStruct.GPIO_Pin = USART3_TX_PIN;
  GPIO_InitStruct.GPIO_Mode = USART3_TX_MODE;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(USART3_TX_GPIO_PORT, &GPIO_InitStruct);
  /* USART3 Rx PB11 */
  GPIO_InitStruct.GPIO_Pin = USART3_RX_PIN;
  GPIO_InitStruct.GPIO_Mode = USART3_RX_MODE;
  GPIO_Init(USART3_RX_GPIO_PORT, &GPIO_InitStruct);

  /* USART3 Init *****************************************************************/
  USART_InitStruct.USART_BaudRate = USART3_BAUDRATE;
  USART_InitStruct.USART_WordLength = USART3_BYTESIZE;
  USART_InitStruct.USART_StopBits = USART3_STOPBITS;
  USART_InitStruct.USART_Parity = USART3_PARITY;
  USART_InitStruct.USART_HardwareFlowControl = USART3_HARDWARECTRL;
  USART_InitStruct.USART_Mode = USART3_Mode;
  USART_Init(USART3, &USART_InitStruct);
  USART_Cmd(USARTx3, ENABLE);
  }
/**************************实现函数********************************************
*函数原型:		void UART3NVIC_Configuration(void)
*功　　能:		初始化USART3中断优先级
*******************************************************************************/
void UART3NVIC_Configuration(void){

        NVIC_InitTypeDef NVIC_InitStructure; 
          /* Enable the USART3 Interrupt */
        NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
//        NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
        NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
        NVIC_Init(&NVIC_InitStructure);
		    USART_ITConfig(USART3, USART_IT_TXE, ENABLE);
	      USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
}
/*******************************************************************************
* Function Name  : int fputc(int ch, FILE *f)
* Description    : Retargets the C library printf function to the USART.fgetc重定向
* Input          : None
* Output         : None
* Return         : 发送字符
*******************************************************************************/
int fputc(int ch, FILE *f)
{
  USART_SendData(USART1, (uint8_t) ch);
  while(!USART_GetFlagStatus(USART1, USART_FLAG_TC));//USART_FLAG_TC
  return ch;
}
/*******************************************************************************
* Function Name  : int fgetc(FILE *f)
* Description    : Retargets the C library printf function to the USART.fgetc重定向
* Input          : None
* Output         : None
* Return         : 读取到的字符
*******************************************************************************/
int fgetc(FILE *f)
{
  /* Loop until received a char */
  while(!(USART_GetFlagStatus(USART1, USART_FLAG_RXNE) == SET));
  /* Read a character from the USART and RETURN */
  return (USART_ReceiveData(USART1));
}

